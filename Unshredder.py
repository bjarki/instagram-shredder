from PIL import Image

def get_shred_width(img):
    lastDiff, diffs = 0, []
    imgw, imgh = img.size
    for pix in range(imgw - 2):
        slice1 = img.crop((pix, 0, pix + 1, imgh))
        slice2 = img.crop((pix + 1, 0, pix + 2, imgh))
        total = get_line_diff(slice1, slice2)

        if lastDiff * 1.6 < total:
            diffs.append(pix)
        lastDiff = total

        if len(diffs) > 3:
            print "We have enough data"
            break

    diffs.reverse()
    diffs.pop(-1) # remove the 0

    w = diffs.pop(-1) + 1

    if  (w * 2) - 1 == diffs.pop():
        print "Width is " + str(w)
        return w
    else:
        print "don't know the width :/"
        return 0

def get_diff_value(img1, img2):
    i1_w, i1_h = img1.size
    slice1 = img1.crop((i1_w - 1, 0, i1_w, i1_h))
    slice2 = img2.crop((0, 0, 1, i1_h))
    return get_line_diff(slice1, slice2)

def get_line_diff(l1, l2):
    total = 0
    for pix in range(l1.size[1]):
        item1 = sum(l1.getpixel((0, pix)))
        item2 = sum(l2.getpixel((0, pix)))
        total += abs(item1 - item2)
    return total

def get_next_img_index(img_part, imgs):
    return_value, loc = 200000, 99
    for inx, img in enumerate(imgs):
         diff = get_diff_value(img_part, img)
         if diff < return_value:
            return_value = diff
            loc = inx

    if img_part == imgs[loc]:
        return -1
    return loc

def get_first_img_index(imgs):
    arr = []
    for inx in range(len(imgs)):
        arr.append(get_next_img_index(imgs[inx], imgs))

    for i in range(len(imgs)):
        if i not in arr:
            return i


image = Image.open('file.png')
width, height = image.size
shred_width = get_shred_width(image)
number_of_columns = width / shred_width
parts = []
unshredded = Image.new("RGBA", image.size)

for p in range(number_of_columns):
    x1, y1 = shred_width * p, 0
    x2, y2 = x1 + shred_width, height
    source_region = image.crop((x1, y1, x2, y2))
    parts.append(source_region)

# get the first img and add it to the list
nxt = get_first_img_index(parts)
imgArr = []
imgArr.append(parts.pop(nxt))
# add the rest of the images
while len(imgArr) != number_of_columns:
    rinx = get_next_img_index(imgArr[-1], parts)
    imgArr.append(parts.pop(rinx))

# Create the image
for idx, img in enumerate(imgArr):
    unshredded.paste(img, (idx * shred_width , 0))

# Output the new image
unshredded.save("unshredded.jpg",  "JPEG")